# WPI Master's project
# David Berthiaume
# Professor Paffenroth

import tensorflow as tf
from tensorflow import keras
import pandas as pd
import seaborn as sb

import numpy as np
import matplotlib.pyplot as p

sb.set()

doRandom = False
#lrate = 0.02
lrate = 0.015
nepochs = 10
timages = 10000


print('Tensor flow version is',tf.__version__)

fashion_mnist = keras.datasets.mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

class_names = ['0', '1', '2', '3', '4',
               '5', '6', '7', '8', '9']

train_images = train_images / 255.0
test_images = test_images / 255.0


train_images = train_images.reshape((60000, 784))[:timages, :]
test_images = test_images.reshape((10000, 784))
train_labels = train_labels[:timages]

test_images = test_images[:1000, :]
test_labels = test_labels[:1000]

if doRandom:

    train_images = np.random.random( train_images.shape)
    test_images = np.random.random( test_images.shape)

print ('running model 1 MLP')

# This returns a tensor
inputs = keras.Input(shape=(784,))
x = keras.layers.Dense(784, activation='sigmoid')(inputs)
x2 = keras.layers.Dense(784, activation='sigmoid')(x)
x3 = keras.layers.Dense(784, activation='sigmoid')(x2)
predictions = keras.layers.Dense(10, activation='softmax')(x3)

model = keras.Model(inputs=inputs, outputs=predictions)
model.compile(optimizer=tf.train.GradientDescentOptimizer(learning_rate=lrate),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

hist1 = model.fit(train_images, train_labels, epochs=nepochs)
test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy:', test_acc)

ac1 = hist1.__dict__['history']['acc']


print ('running model 2 residual')



inputs = keras.Input(shape=(784,))
x = keras.layers.Dense(784, activation='sigmoid')(inputs)

x2 = keras.layers.Dense(784, activation='sigmoid')(x)
y2 = keras.layers.add([x2, x])
x3 = keras.layers.Dense(784, activation='sigmoid')(y2)
y3 = keras.layers.add([x3, x2])
predictions = keras.layers.Dense(10, activation='softmax')(y3)

model = keras.Model(inputs=inputs, outputs=predictions)
model.compile(optimizer=tf.train.GradientDescentOptimizer(learning_rate=lrate),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

his2 = model.fit(train_images, train_labels, epochs=nepochs)
test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy:', test_acc)

ac2 = his2.__dict__['history']['acc']

print ('running model 3 add input')


inputs = keras.Input(shape=(784,))
x = keras.layers.Dense(784, activation='sigmoid')(inputs)

x2 = keras.layers.Dense(784, activation='sigmoid')(x)
y2 = keras.layers.add([x2, inputs])
x3 = keras.layers.Dense(784, activation='sigmoid')(y2)
y3 = keras.layers.add([x3, inputs])
predictions = keras.layers.Dense(10, activation='softmax')(y3)

model = keras.Model(inputs=inputs, outputs=predictions)
model.compile(optimizer=tf.train.GradientDescentOptimizer(learning_rate=lrate),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

his3 = model.fit(train_images, train_labels, epochs=nepochs)
test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy:', test_acc)

ac3 = his3.__dict__['history']['acc']

df = pd.DataFrame({'MLP':ac1, 'Residual':ac2, 'Add Inputs':ac3})

df.to_csv("res/mnistmany")

print (df)

sb.lineplot(range(len(ac1)), ac1, markers = 'o', label = 'MLP')
sb.lineplot(range(len(ac1)), ac2, markers = 'o', label = 'Residual')
sb.lineplot(range(len(ac1)), ac3, markers = 'o', label = 'Add Inputs')
p.title('Training accuracy vs epoch, number of inputs = ' + str(timages))
p.xlabel('epoch')
p.ylabel('training accuracy')
p.savefig('res/fitnoisemany' + str(timages) + '.pdf')